import { TweenLite, TweenMax } from "gsap";
import {BasicScene} from "./scene";
const NUMBER_OF_INSTANCE = 144;
export class Scene1 extends BasicScene{
    private bunniesStack1 : PIXI.Container;
    private tweenArr:TweenLite[] = [];
    constructor(public app:any){
        super(app);
        
    }
    start(){
        this.bunniesStack1 = new PIXI.Container();
        for(let i = 0; i < NUMBER_OF_INSTANCE; i++){

            let bunny = new PIXI.Sprite(PIXI.loader.resources.bunny.texture);

            bunny.position.set(this.app.initialWidth *0.5-100, this.app.initialHeight * 0.8- 5*i);
            bunny.scale.set(3.0);
            this.bunniesStack1.addChild(bunny);

        }
        this.bunniesStack1.children.forEach((element, i) =>{
            let start = {x: element.x, y: element.y}
            let target = {x:this.app.initialWidth *0.5+100, y: this.app.initialHeight * 0.8- 5*i}
            let tweenStart = TweenLite.to(element, 2, {x:target.x, y:target.y, onStart: ()=>{
                element.zIndex = NUMBER_OF_INSTANCE-i;
                this.bunniesStack1.children.sort((itemA, itemB) => itemA.zIndex - itemB.zIndex);
            }});
            tweenStart.startTime((NUMBER_OF_INSTANCE-i));
            
        })
    
        this.addChild(this.bunniesStack1);
        
    }
    exit(){
        this.bunniesStack1.children.forEach(element=>{
            TweenLite.killTweensOf(element);
        })

        this.bunniesStack1.removeChildren();
    }
}