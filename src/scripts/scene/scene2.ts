import {random, shuffle} from "../util"
import { TextStyle } from "pixi.js";
import { TweenLite } from "gsap";
import {BasicScene} from "./scene"
import { listLanguages } from "highlight.js";
const CONST_TEXT1 = ["Hello", "Bello", "Hi","Holla", "Welcome"]

const PADDING = 10;
export class Scene2 extends BasicScene{
    private interval : any;
    private textContainer: PIXI.Container = new PIXI.Container();
    private listContents: any[] = [];
    private style : PIXI.TextStyle;
    constructor(public app:any){
        super(app);
        
        this.addChild(this.textContainer);
        this.style = new PIXI.TextStyle({
            fontFamily: "Verdana",
            fontSize: random(10, 50),
            fill: "#FFFFFF",
            wordWrap: true,
            wordWrapWidth: 440,
        });
        CONST_TEXT1.forEach(()=>{
            let text = new PIXI.Text(CONST_TEXT1[random(0, CONST_TEXT1.length-1)], this.style);
            if(text)
            this.listContents.push(text);
        })
        for(let i = 1; i <= 5; i++){
            let emoji = new PIXI.Sprite(PIXI.loader.resources['emoji'+i].texture);
            if(emoji)
            this.listContents.push(emoji);
        }
        
    }
    start(){
        this.interval = setInterval(()=>{
            this.textContainer.removeChildren();
            this.textContainer.width = 0;
            shuffle(this.listContents);
            this.loop();
        }, 2000);
    }
    loop(){
        
        let text1 = this.listContents[0];
        let ORG  = {x:this.app.initialWidth *0.5, y:this.app.initialHeight * 0.5}
        this.randomSize(text1);
        text1.anchor.set(0, 1);
        text1.position.set(ORG.x, ORG.y);
        this.textContainer.addChild(text1);
        let text2 = this.listContents[1];
        this.randomSize(text2);
        text2.anchor.set(0,1);
        text2.position.set(ORG.x + this.textContainer.width + PADDING, ORG.y);
        this.textContainer.addChild(text2);
        let text3 = this.listContents[2];
        this.randomSize(text3);
        text3.anchor.set(0, 1);
        text3.position.set(ORG.x + this.textContainer.width + PADDING, ORG.y);
        this.textContainer.addChild(text3);
    }
    randomSize(text: PIXI.Text){
        if(text.style){

            text.style.fontSize = random(10, 50);
        }
    }
    exit(){
        if(this.interval){
            clearInterval(this.interval);
            this.interval = null;
        }

    }
}