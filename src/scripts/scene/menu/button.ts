import { AlignMiddle } from "vendor/dacaher/pixi-app-wrapper/stage/align/align-middle";

export class Button extends PIXI.Sprite {
    constructor(texture: PIXI.Texture, content?:string){
        super(texture);
        this.buttonMode = true;
        this.interactive = true;
        let style  = new PIXI.TextStyle({
            fontFamily: "Verdana",
            fontSize: this.width/15,
            fill: "#FFFFFF",
            wordWrap: true,
            wordWrapWidth: 440,
        });
        let text = new PIXI.Text(content, style);
        this.pivot.set(this.width/2, this.height/2);
       
        text.anchor.set(0.5, 0.5);
        this.addChild(text);
    }

}