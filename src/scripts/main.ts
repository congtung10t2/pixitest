import {TweenLite} from "gsap";
import {Asset, AssetPriority, LoadAsset, PixiAssetsLoader, SoundAsset} from "pixi-assets-loader";
import {Scene1} from "./scene/scene1"
import {Scene2} from "./scene/scene2"
import {Scene3} from "./scene/scene3"
import {MenuScene} from "./scene/menuScene";
import {
    Dom,
    PixiAppWrapper as Wrapper,
    pixiAppWrapperEvent as WrapperEvent,
    PixiAppWrapperOptions as WrapperOpts,
} from "pixi-app-wrapper";
export class DemoApp {
    private app: Wrapper;
    private totalAssets: number;
    private loadingProgress: number;
    private screenBorder: PIXI.Graphics;
    private assetsCount: { [key: number]: { total: number, progress: number } } = {};
    private loader: PixiAssetsLoader;
    private currentScene: any;
    constructor(){
        const canvas = Dom.getElementOrCreateNew<HTMLCanvasElement>("app-canvas", "canvas", document.getElementById("app-root"));

        // if no view is specified, it appends canvas to body
        const appOptions: WrapperOpts = {
            width: 1920,
            height: 1080,
            scale: "keep-aspect-ratio",
            align: "middle",
            resolution: window.devicePixelRatio,
            roundPixels: true,
            transparent: false,
            backgroundColor: 0x000000,
            view: canvas,
            showFPS: true,
            showMediaInfo: true,
            changeOrientation: true,
        };

        this.app = new Wrapper(appOptions);
        this.app.on(WrapperEvent.RESIZE_START, this.onResizeStart.bind(this));
        this.app.on(WrapperEvent.RESIZE_END, this.onResizeEnd.bind(this));
        const assets = [
            {id: "bunny", url: "assets/gfx/bunny.png", priority: AssetPriority.HIGH, type: "texture"},
            {id: "emoji1", url: "assets/emoji/1.png", priority: AssetPriority.HIGH, type: "texture"},
            {id: "emoji2", url: "assets/emoji/2.png", priority: AssetPriority.HIGH, type: "texture"},
            {id: "emoji3", url: "assets/emoji/3.png", priority: AssetPriority.HIGH, type: "texture"},
            
            {id: "emoji4", url: "assets/emoji/4.png", priority: AssetPriority.HIGH, type: "texture"},
            {id: "emoji5", url: "assets/emoji/5.png", priority: AssetPriority.HIGH, type: "texture"},
            {id: "particle", url: "assets/gfx/particle.png", priority: AssetPriority.HIGH, type: "texture"},
            {id: "fire", url: "assets/gfx/Fire.png", priority: AssetPriority.HIGH, type: "texture"},
            {id: "button", url: "assets/gfx/button.png", priority: AssetPriority.HIGH, type: "texture"},
            {id: "home", url: "assets/gfx/home.png", priority: AssetPriority.HIGH, type: "texture"}
    
        ]
        assets.forEach(asset => {
            if (!this.assetsCount[asset.priority]) {
                this.assetsCount[asset.priority] = {total: 1, progress: 0};
            } else {
                this.assetsCount[asset.priority].total++;
            }
         });
        this.loadingProgress = 0;
        this.totalAssets = assets.length;
        this.loader = new PixiAssetsLoader();
        this.loader.on(PixiAssetsLoader.ALL_ASSETS_LOADED, this.onAllAssetsLoaded.bind(this));

        this.loader.addAssets(assets).load();
    }
    private onAllAssetsLoaded(): void {
        window.console.log("[SAMPLE APP] onAllAssetsLoaded !!!!");
        this.onGameStart();
    }
    private onGameStart(){
        let menuScene: MenuScene = new MenuScene(this.app);
        this.app.stage.addChild(menuScene);
        this.currentScene = menuScene;
        this.initCommand();
        
    }
    private enterScene(scene:any){
        if(this.currentScene){
            this.currentScene.exit();
            
            this.app.stage.removeChild(this.currentScene);
        
        }
        this.currentScene = scene;
        scene.start();
        this.app.stage.addChild(this.currentScene);
        this.currentScene.on("exit", ()=>{
            this.exitScene();
        })
    }
    private exitScene(){
        if(this.currentScene){
            this.currentScene.exit();
            
            this.app.stage.removeChild(this.currentScene);
        
        }
        this.currentScene = new MenuScene(this.app);
        this.currentScene.start();
        this.app.stage.addChild(this.currentScene);
        this.initCommand();

    }
    private initCommand(){
        if(!this.currentScene)
        return;
        this.currentScene.on("animation", ()=>{
            this.enterScene(new Scene1(this.app));
        });
        this.currentScene.on("imageTool", ()=>{
            this.enterScene(new Scene2(this.app));
        });
        this.currentScene.on("particle", ()=>{
            this.enterScene(new Scene3(this.app));
        });
    }

    private onResizeStart(): void {
        window.console.log("RESIZE STARTED!");
    }

    private onResizeEnd(args: any): void {
        window.console.log("RESIZE ENDED!", args);

     
    }
    private relocateViews(): void {
    }
}