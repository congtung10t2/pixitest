import {Button} from "./menu/button"
export interface Scene{
    start(): void;
    exit(): void;
}
export class BasicScene extends PIXI.Container implements PIXI.utils.EventEmitter{
   constructor(public app?:any){
       super();
       let exitButton = new Button(PIXI.loader.resources.home.texture);
        
       exitButton.scale.set(0.3,0.3);
       exitButton.position.set( this.app.initialWidth -  exitButton.width,  this.app.initialHeight - exitButton.height);
       exitButton.on("pointerdown", ()=>{
            this.emit("exit")
        })
        this.addChild(exitButton);
   }
}