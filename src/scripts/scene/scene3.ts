import "pixi-particles";
import {BasicScene} from "./scene"
export class Scene3 extends BasicScene{
    private particlesEmitter: PIXI.particles.Emitter;
    private particlesContainer: PIXI.particles.ParticleContainer;
    private timer: number;
    
    constructor(public app:any){
        super(app);
    }
    start(){
        this.particlesContainer = new PIXI.particles.ParticleContainer();
        this.particlesContainer.position.set(this.app.initialWidth *0.5, this.app.initialHeight * 0.5);
        this.app.stage.addChild(this.particlesContainer);
        this.particlesEmitter = new PIXI.particles.Emitter(this.particlesContainer, [PIXI.loader.resources.particle.texture, PIXI.loader.resources.fire.texture], {
            alpha: {
                start: 0.62,
                end: 0,
            },
            scale: {
                start: 0.25,
                end: 0.75,
            },
            color: {
                start: "fff191",
                end: "ff622c",
            },
            speed: {
                start: 500,
                end: 500,
            },
            startRotation: {
                min: 265,
                max: 275,
            },
            rotationSpeed: {
                min: 50,
                max: 50,
            },
            lifetime: {
                min: 0.1,
                max: 0.75,
            },
            frequency: 0.001,
            blendMode: "normal",
            emitterLifetime: 0,
            maxParticles: 1000,
            pos: {
                x: 0,
                y: 0,
            },
            addAtBack: false,
            spawnType: "circle",
            spawnCircle: {
                x: 0,
                y: 0,
                r: 10,
            },
            emit: false,
            autoUpdate: true,
        });
        
        let elapsed = Date.now();
        this.startEmittingParticles();
        this.timer = Date.now();

        this.app.ticker.add(this.update)

    }
    update(){
        // Update the next frame
        // requestAnimationFrame(update);
        const now = Date.now();
        // The emitter requires the elapsed
        // number of seconds since the last update
        if (this.particlesEmitter)
        this.particlesEmitter.update((now - this.timer) * 0.001);
        this.timer = now;
    };
    private stopEmittingParticles(): void {
        if (this.particlesEmitter) {
            this.particlesEmitter.emit = false;
            this.particlesEmitter.cleanup();
        }
    }
    exit(){
        this.stopEmittingParticles();
        this.app.ticker.remove(this.update);
    }
    private startEmittingParticles(): void {
        if (this.particlesEmitter) {
            this.particlesEmitter.emit = true;
        }
    }
}