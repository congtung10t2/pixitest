/**
 * App bundle entry point.
 */
import "styles/style.css"; // Required to bundle styles!
import * as app from "./main"
const demo = new app.DemoApp();