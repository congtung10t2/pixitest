import {Button} from "./menu/button"
import {Scene} from "./scene"
export class MenuScene extends PIXI.Container implements PIXI.utils.EventEmitter{
    private animationMoving : PIXI.Sprite;
    private imageTool: PIXI.Sprite;
    private particles: PIXI.Sprite;
    constructor(public app?:any){
        super();
        let animationStack = new Button(PIXI.loader.resources.button.texture, "Animation");
        this.addChild(animationStack);
        animationStack.position.set( this.app.initialWidth/2,  this.app.initialHeight/4);
        
        animationStack.anchor.set(0.5, 0.5);
        animationStack.scale.set(0.3,0.3);
        animationStack.on("pointerdown", ()=>{
            this.emit("animation");
        });
        let imageTool = new Button(PIXI.loader.resources.button.texture, "ImageTool");
        this.addChild(imageTool);
        imageTool.scale.set(0.3,0.3);
        imageTool.position.set( this.app.initialWidth/2,  this.app.initialHeight/2);
        imageTool.on("pointerdown", ()=>{
            this.emit("imageTool")
        })
        imageTool.anchor.set(0.5, 0.5);
        let particles = new Button(PIXI.loader.resources.button.texture, "Particles");
        this.addChild(particles);
        particles.scale.set(0.3,0.3);
        particles.position.set( this.app.initialWidth/2,  this.app.initialHeight*3/4);
        particles.on("pointerdown", ()=>{
            this.emit("particle")
        })
        particles.anchor.set(0.5, 0.5);
    }
    start(){
        
    }
    exit(){

    }

}